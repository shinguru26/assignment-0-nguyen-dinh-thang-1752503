
package net.codejava;
import java.util.Scanner;

public class Warrior {
	private int baseHP,wp;
	
	public Warrior() {
		baseHP = 0;
		wp = 0;
	}
	
	public Warrior(int baseHP,int wp) {
		setBaseHP(baseHP);
		setwp(wp);
	}
		
	public void setBaseHP(int baseHP) {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		while ((baseHP < 1) || (baseHP > 888)) {
			System.out.println("Invalid HP for warrior! Must be from 1 to 888:");
			baseHP = sc.nextInt();
		}
		this.baseHP = baseHP;
	}
	
	public void setwp(int wp) {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		while ((wp < 0) || (wp > 3)){
			System.out.println("Invalid weapon for warrior! Must be from 0 to 3:");
			wp = sc.nextInt();
		}
		this.wp = wp;
	}
	
	public int getBaseHP() {
		return baseHP;
	}
	
	public int getwp() {
		return wp;
	}
	
	public int getRealHP() {
		if (this.wp == 0) return (this.baseHP/10); 
		else return (this.baseHP); 
	}
	
	public String OddofKnightVictory(Knight knight) {
		double odd = Math.round(((knight.getRealHP() - this.getRealHP() + 999.0)/2000.0)*10000)/100.0;
		return odd + "%";
	}
}
